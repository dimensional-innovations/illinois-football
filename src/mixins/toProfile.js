export const toProfile = {
  methods: {
    toProfile(idx, id) {
      this.$refs.player[idx].classList.add("active");
      this.$socket.sendObj({ id });
      setTimeout(() => {
        this.$router.push(`/profile/${id}`);
      }, 200);
    }
  }
};
