import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Seasons from "../views/Seasons.vue";
import Season from "../views/Season.vue";
import Category from "../views/Category.vue";
import Profile from "../views/Profile.vue";
import Settings from "../views/Settings.vue";
import store from "../store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/seasons",
    name: "Seasons",
    component: Seasons,
		beforeEnter: (to, from, next) => {
			store.commit("setDecades");
			next();
		}
  },
  {
    path: "/seasons/:season",
    name: "Season",
    component: Season,
    beforeEnter: (to, from, next) => {
			const category = store.getters.activeSection(to.path);
			const payload = { awardID: category.id[0], season: to.params.season };
      store.commit("emptyAwardWinners");
			store.dispatch("fetchSeason", payload).then(() => {
				next();
			});   
    }
  },
  {
    path: "/:category",
    name: "Category",
    component: Category,
    beforeEnter: (to, from, next) => {
			const category = store.getters.activeSection(to.path);
			store.commit("setLoading", true);
			store.commit("emptyAwardWinners");
			store.dispatch("fetchCategory", category.id).then(() => {
				next();
			});
    }
  },
  {
    path: "/profile/:id",
    name: "Profile",
    component: Profile,
    beforeEnter: (to, from, next) => {
			const payload = { name: from.name, params: from.params };
			store.commit("setProfileText", payload);
			store.dispatch("setProfilePlayer", to.params.id).then(() => {
				next();
			});
    }
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings
  }
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  store.commit("setShowVideoModal", false);
  next();
})

export default router;