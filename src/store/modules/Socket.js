import Vue from "vue";

const state = {
  socket: {
    isConnected: false,
    reconnectError: false,
    message: ""
  }
};

const mutations = {
  SOCKET_ONOPEN(state, event) {
    Vue.prototype.$socket = event.currentTarget;
    state.socket.isConnected = true;
  },
  SOCKET_ONCLOSE(state, event) {
    state.socket.isConnected = false;
  },
  SOCKET_ONERROR(state, event) {
    console.error(state, event);
  },
  SOCKET_ONMESSAGE(state, message) {
    console.log("KIOSK SOCKET_ONMESSAGE", message);
    state.socket.message = message;
  },
  SOCKET_RECONNECT(state, count) {
    console.info(state, count);
  },
  SOCKET_RECONNECT_ERROR(state) {
    state.socket.reconnectError = true;
  }
};

const actions = {
  sendMessage: function(context, message) {
    Vue.prototype.$socket.send(message);
  }
};

export default {
  state,
  mutations,
  actions
};
