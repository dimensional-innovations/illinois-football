import Vue from "vue";
import Vuex from "vuex";
import _, { lowerFirst } from "lodash";
import Axios from "axios";
import { setupCache } from "axios-cache-adapter";

Vue.use(Vuex);

const cache = setupCache({
  maxAge: 60 * 60 * 1000,
  exclude: { query: false }
});
const axios = Axios.create({ adapter: cache.adapter });
const baseURL = "http://illinois.dimin.io/api/football/v2/";
const baseAppYear = 1890;
const latestAppYear = new Date().getUTCFullYear() - 1;

const api = {
  state: {
    loading: false,
    categories: [
      { name: "letterwinners", path: "letterwinners", id: [1] },
      { name: "all-americans", path: "all-americans", id: [2] },
      { name: "all b1g", path: "all-b1g", id: [3, 4, 5, 8, 13] },
      { name: "seasons", path: "seasons", id: [1] }
    ],
    decades: [],
    awardTypes: [1, 2, 3, 4, 5, 8, 13],
    awards: [],
    awardWinners: [],
    profileText: "",
    profilePlayer: {},
    recruitingVideos: [],
    groupedPlayers: null,
    showModal: false,
    projector: {
      cycleAwardIndex: 0,
      cycleAwardTypes: ["letter_winners", "all_americans", "all_b1g"],
      activeCycleAward: "letter_winners",
      activeLetterWinnerDecade: null
    }
  },
  mutations: {
    setPlayer: (state, data) => (state.profilePlayer = data),
    setProfileText: (state, data) => {
      if (data.name === "Season") {
        state.profileText = `${data.params.season} season`;
      } else {
        state.profileText = state.categories.find(category => category.path.indexOf(data.params.category) != -1).name;
      }
    },
    setAwardWinners: (state, data) => (state.awardWinners = data),
    setDecades(state) {
      let years = [];
      let decades;

      for (let y = baseAppYear; y <= latestAppYear; y++) {
        years.push(y);
      }

      decades = _.chunk(years, 10).reverse();

      decades.forEach(decade => {
        decade.sort((a, b) => b - a);
      });

      state.decades = decades;
    },
    setShowVideoModal: (state, data) => (state.showModal = data),
    emptyAwardWinners: (state) => (state.awardWinners = []),
    updateCycleAward(state) {
      state.projector.cycleAwardIndex = state.projector.cycleAwardIndex < state.projector.cycleAwardTypes.length - 1 ? state.projector.cycleAwardIndex += 1 : 0;
      state.projector.activeCycleAward = state.projector.cycleAwardTypes[state.projector.cycleAwardIndex];
      state.projector.activeLetterWinnerDecade = Object.keys(state.groupedPlayers["letter_winners"])[0];
    },
    setCycleLetterWinnerDecade: (state, data) => (state.projector.activeLetterWinnerDecade = data),
    setLoading: (state, data) => (state.loading = data),
    set_recruitingvideos: (state, data) => (state.recruitingVideos = data),
    set_awards(state, data) {
      state.awards = data;

      const baseAppYear = 1890;
      const latestAppYear = new Date().getUTCFullYear() - 1;
      const b1gIDs = [3, 4, 5, 8, 13];
      let players = {};

      players["letter_winners"] = {};
      players["all_americans"] = data.filter(player => player.award_type.id === 2);
      players["all_b1g"] = data.filter(player => b1gIDs.indexOf(player.award_type.id) !== -1);

      const letterWinners = data.filter(player => player.award_type.id === 1);

      for (let y = baseAppYear; y <= latestAppYear; y++) {
        if (y % 10 === 0) players["letter_winners"][y] = [];
      }

      Object.keys(players["letter_winners"]).forEach((key, idx) => {
        players["letter_winners"][key] = letterWinners.filter(player => player.year >= parseInt(key) && player.year < parseInt(key) + 10);
      });
      
      state.groupedPlayers = players;
      state.projector.activeLetterWinnerDecade = Object.keys(state.groupedPlayers.letter_winners)[0];
    }
  },
  actions: {
    setProfilePlayer: async ({ commit, state }, id) => {
      return new Promise((resolve, reject) => {
        const awardInfo = state.awards.filter(d => d.player.id === parseInt(id));
        const playerData = {
          playerInfo: awardInfo[0].player,
          awardInfo
        };
        commit("setPlayer", playerData);
        resolve();
      });
    },
    fetchSeason: async ({ commit, state }, payload) => {
      return new Promise((resolve, reject) => {
        let filteredData = state.awards.filter(d => d.award_type.id === parseInt(payload.awardID) && d.year === parseInt(payload.season));

        filteredData = filteredData.sort((a, b) => {
          if (a.player.last_name > b.player.last_name) return 1;
          if (a.player.last_name < b.player.last_name) return -1;
          return 0;
        });

        commit("setAwardWinners", filteredData);
        resolve();
      });
    },
    fetchCategory({ commit, state }, awardIDs) {
      return new Promise((resolve, reject) => {
        let people = state.awards.filter(d => awardIDs.indexOf(d.award_type.id) > -1);

        people = _.uniqBy(people, function(person) {
          return person.player.id;
        });

        people.sort((a, b) => {
          if (a.player.last_name > b.player.last_name) {
            return 1;
          } else if (a.player.last_name < b.player.last_name) {
            return -1;
          } else {
            return 0;
          }
        });

        commit("setAwardWinners", people);
        commit("setLoading", false);
        resolve();

        // axios.get(`${baseURL}awards/`).then(response => {
        //   let people = response.data.filter(d => awardIDs.indexOf(d.award_type.id) > -1);

        //   people = _.uniqBy(people, function(person) {
        //     return person.player.id;
        //   });

        //   people.sort((a, b) => {
        //     if (a.player.last_name > b.player.last_name) {
        //       return 1;
        //     } else if (a.player.last_name < b.player.last_name) {
        //       return -1;
        //     } else {
        //       return 0;
        //     }
        //   });

        //   commit("setAwardWinners", people);
        //   commit("setLoading", false);
        //   resolve();
        // });
      });
    }
  },
  getters: {
    activeSection: state => $route => {
      return state.categories.filter(category => $route.replace("/", "").indexOf(category.path) != -1)[0];
    }
  }
};

export default api;