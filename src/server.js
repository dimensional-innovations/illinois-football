import WebSocket from "ws";
import settings from "electron-settings";

class Server {
  constructor() {
    const port = settings.getSync("config.websocket.port");
    const wss = new WebSocket.Server({ port });
    wss.on("connection", function connection(ws) {
      ws.on("message", function incoming(data) {
        wss.clients.forEach(client => {
          if (client !== ws && client.readyState === WebSocket.OPEN) {
            client.send(data);
          }
        });
      });
    });
  }
}

export default new Server();