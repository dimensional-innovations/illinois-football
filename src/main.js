import Vue from "vue";
import VueAnalytics from "vue-analytics";
import Vue2TouchEvents from "vue2-touch-events";
import VueNativeSock from "vue-native-websocket";
import axios from "axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import server from "./server.js";
import { remote, webFrame } from "electron";
import { Heartbeat, Modes } from "@dimensional-innovations/node-heartbeat";
import settings from "electron-settings";

const config = settings.getSync("config");

Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

Vue.use(Vue2TouchEvents, {
  longTapTimeInterval: 1500
});

Vue.use(
  VueNativeSock,
  `ws://${config.websocket.host}:${config.websocket.port}`,
  {
    format: "json",
    store,
    reconnection: true
  }
);

Vue.use(VueAnalytics, {
  id: "UA-142853029-1",
  router,
  set: [
    { field: "checkProtocolTask", value: null },
    { field: "checkStorageTask", value: null }
  ],
  autoTracking: {
    pageviewTemplate(route) {
      return {
        page: `/illinois-football/${route.name}`,
        title: `Illinois Football`,
        location: `/illinois-football${route.path}`
      };
    }
  },
  debug: {
    enabled: false
  }
});

const requireComponent = require.context(
  "./components",
  false,
  /VideoModal\.(vue|js)$/
);

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName);
  const componentName = fileName.replace(/^\.\/(.*)\.\w+$/, "$1");
  Vue.component(componentName, componentConfig.default || componentConfig);
});

new Vue({
  router,
  store,
  render: function(h) {
    return h(App);
  }
}).$mount("#app");

webFrame.setVisualZoomLevelLimits(1, 1);

document.addEventListener("contextmenu", e => {
  e.preventDefault();
  return false;
});

new Heartbeat({
  apiKey: null, // TODO: go create a new hearbeat token at https://betteruptime.com/team/8248/heartbeats
  enabled: remote.app.isPackaged,
  mode: Modes.BROWSER,
}).start();
