import { init } from '@dimensional-innovations/vue-electron-background';
import { app } from 'electron';
import { config } from '../package';

const { isPackaged } = app;

init({
  enableKioskMode: isPackaged,
  enableAutoUpdater: isPackaged,
  config,
});
