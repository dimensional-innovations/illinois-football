module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        orange: "#e04e39",
        blue: "#13294b",
        gray: "#a5acaf"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
